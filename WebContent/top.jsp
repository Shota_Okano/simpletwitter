<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<script src="./js/jquery-3.6.0.js"></script>
		<script src="./js/jquery-3.6.0.min.js"></script>
		<script src="./js/main.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>簡易Twitter</title>
	</head>
	<body>
		<div class="main-contents">
			<!-- OK -->
			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">新規登録</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="setting">設定</a>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>

			<!-- OK -->
			<c:if test="${ not empty loginUser }">
				<div class="profile">
					<div class="name"><h2><c:out value="${ loginUser.name }" /></h2></div>
					<div class="account">@<c:out value="${ loginUser.account }" /></div>
					<div class="discription"><c:out value="${ loginUser.description }" /></div>

				</div>
			</c:if>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="calendar">
				<form action="./" method="get">
					<span>
						日付：
						<input type="date" name="startDate" value="<c:out value="${startDate}"/>">
						<span>　～　</span>
						<input type="date" name="endDate" value="<c:out value="${endDate}"/>">
						<input type="submit" value="絞り込み" />
					</span>
				</form>
			</div>

			<!-- tweetフォーム -->
			<div class="form-area">
				<c:if test="${ isShowMessageForm }">
					<form action="message" method="post">
						いま、どうしてる？<br />
						<textarea name="text" cols="100" rows="5" wrap="hard" class="tweet-box">${ text }</textarea><br />
						<input type="submit" value="つぶやく">(140文字まで)
					</form>
				</c:if>
			</div>

			<!-- tweet表示部 -->
			<div class="messages">
				<c:forEach items="${ messages }" var="message">

					<div class="message">
						<div class="account-name">
							<span class="account">
								<a href="./?user_id=<c:out value="${ message.userId }"/>">
									<c:out value="${ message.account }" />
								</a>
							</span>
							<span class="name"><c:out value="${ message.name }" /></span>
						</div>
						<div class="text"><c:out value="${ message.text }" /></div>
						<div class="date"><fmt:formatDate value="${ message.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>

						<c:if test="${ loginUser.id == message.userId }">
							<div class="edit-links">
								<form action="edit" method="get" class="edit-button">
									<input type="hidden" name="messageId" value="${ message.id }" />
									<input type="submit" value="編集" />
								</form>
								<form action="deleteMessage" method="post" class="delete-button">
									<input type="hidden" name="messageId" value="${ message.id }" />
									<input type="submit" value="削除" />
								</form>
							</div>
						</c:if>
						<!-- 返信機能追加 -->
						<c:if test="${ isShowMessageForm }">
							<div class="reply-area">
								<form action="comment" method="post">
									<textarea name="text" wrap="hard" cols="90" rows="5" class="reply-box"></textarea><br />
									<input type="hidden" name="messageId" value="${ message.id }" />
									<input type="submit" value="リプライ">
								</form>
							</div>
						</c:if>
						<!-- 返信表示部分 -->
						<c:forEach items="${ comments }" var="comment">
							<div class="comment">
								<c:if test="${ message.id == comment.messageId }">
									<div class="account-name">
										<span class="account">
											<c:out value="${ comment.account }" />
										</span>
										<span class="name"><c:out value="${ comment.name }" /></span>
									</div>
									<div class="text"><c:out value="${ comment.text }" /></div>
									<div class="date"><fmt:formatDate value="${ comment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								</c:if>
							</div>
						</c:forEach>
					</div>
				</c:forEach>
			</div>

			<div class="copyright"> Copyright(c) Shota Okano</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="test.js"></script>
	</body>
</html>