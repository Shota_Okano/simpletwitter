<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>編集画面</title>
	</head>
	<body>
		<div class="header">
				<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">新規登録</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<div class="edit-form">
			<form action="edit" method="post">
				つぶやき<br />
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${ errorMessages }" var="errorMessage">
								<li><c:out value="${ errorMessage }" />
							</c:forEach>
						</ul>
					</div>
				</c:if>
				<textarea name="text" cols="100" rows="5" class="tweet-box">${ message.text }</textarea><br/>
				<input type="hidden" name="messageId" value="${ message.id }" />
				<input type="submit" value="更新" />
			</form>
		</div>

		<a href="./">戻る</a><br/>
		<div class="copyright"> Copyright(c) Shota Okano</div>

	</body>
</html>