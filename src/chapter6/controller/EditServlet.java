package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("messageId");
		Message message = null;
		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		if(!id.isEmpty() && id.matches("^[0-9]*")) {
			message = new MessageService().select(Integer.parseInt(request.getParameter("messageId")));
		}

		//id が数字意外、空白、存在しない場合は message は取得できないため
		if(message == null) {
			errorMessages = new ArrayList<String>();
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		int messageId = Integer.parseInt(request.getParameter("messageId"));
		String text = request.getParameter("text");

		if (isValid(text, errorMessages)) {
			Message message = getMessage(request, messageId);
			new MessageService().update(message);
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages" , errorMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request, int message_id) throws IOException, ServletException {
		Message message = new Message();
		message.setId(message_id);
		message.setText(request.getParameter("text"));
		return message;
	}

	private boolean isValid(String text, List<String> errorMessages) {
		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
