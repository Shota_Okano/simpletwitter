package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public List<UserMessage> select(String userId, String startDate, String endDate) {

		final int LIMIT_NUM = 1000;
		Connection connection = null;
		Timestamp date = Timestamp.valueOf("2020-01-01 00:00:00");
		Timestamp dateToday = new Timestamp(System.currentTimeMillis());;

		if(!StringUtils.isEmpty(startDate)) {
			date = Timestamp.valueOf(startDate + " 00:00:00");
		}

		if(!StringUtils.isEmpty(endDate)) {
			dateToday = Timestamp.valueOf(endDate + " 23:59:59");
		}

		try {
			connection = getConnection();
			Integer id = null;

			if(!StringUtils.isEmpty(userId)) {
				id = Integer.valueOf(userId);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, date, dateToday);
			commit(connection);

			return messages;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//編集機能用（ SELECT ）
	public Message select(int messageId) {
		Connection connection = null;

		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, messageId);
			commit(connection);

			return message;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//編集機能用（ UPDATE )
	public void update(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
